##Contact-Application

A simple contact application which lets you search contacts for keywords.
Will create a new JSON file containing random contacts if no JSON file is present.

Uses a attempt to MVC pattern.

Libraries used: <br/>
* [Java Faker - 1.0.2](https://mvnrepository.com/artifact/com.github.javafaker/javafaker/1.0.2) - Constructing fake data.
* [JSON In Java](https://mvnrepository.com/artifact/org.json/json/20200518) - Storing data.


Andreas Lif