package se.experis.com;

import se.experis.com.controller.Controller;

import java.io.IOException;

public class Program {
    public static void main(String[] args) {
        new Controller().start();
    }
}
