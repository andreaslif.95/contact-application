package se.experis.com.view;

import se.experis.com.model.helper.ProgressRunner;

import java.util.Scanner;

/**
 * Used to print messages to console.
 * Also used to fetch user input.
 * Threads are used for loadingbars.
 */
public class View {

    public void attemptToLoad() {
        System.out.println("Attempting to load contacts from the JSON file.");
    }

    public boolean badJsonObjects() {
        System.out.println("The Json file did not contain the expected object types or were empty. Would you like to override it? (Y/N)");
        return new Scanner(System.in).nextLine().toLowerCase().contains("y");
    }

    public void fileNotFound() {
        System.out.println("\nFile not found");
    }

    public void somethingWentWrong() {
        System.out.println("Something went wrong..");
    }
    public void creatingContactBookEntries() {
        System.out.println("Creating contact book entries..");
    }
    public void attemptingToCreateJson() {
        System.out.println("Creating and saving entries to json.");
    }
    public void fatalErrorExiting() {
        System.out.println("The contacts were faulty.. Contacts could no be saved. Program exit.");
    }

    /**
     * Uses the commonly used static class ProgressRunner to know when the model is done loading so that the thread end.
     * Will print '.' as loading indications.
     */
    public void loadProgress() {
        new Thread(() ->  {
            while(!ProgressRunner.runProgress()) {}
            System.out.print("Loading");
            while(ProgressRunner.runProgress()) {

                System.out.print(".");
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Thread.currentThread().interrupt();
        }).start();
    }

    /**
     * Uses the static class ProgressRunner to know when the creation is done and also to print the correct creation numbers.
     * Prints information about contacts created to console.
     */
    public void creationProgress() {
        new Thread(() ->  {

            while(ProgressRunner.getCount()[0] == -1) {
            }
            int[] counts = ProgressRunner.getCount();
            while(counts[0] < counts[1]) {
                System.out.print("Progress..[" + counts[0] + "/" + counts[1] + "] created.\r");
                while(ProgressRunner.getCount()[0] <= counts[0]) {
                }
                counts = ProgressRunner.getCount();
            }
            Thread.currentThread().interrupt();
        }).start();
    }
    public void successfullyLoaded() {
        cleanScreen();
        System.out.println("Everything was sucessfully loaded!");
    }

    /**
     * Used to clean screen. Basically prints spaces and new lines to clean the screen.
     */
    private void cleanScreen() {
        for(int i = 0; i < 400; i++) {
            System.out.println(" ");
        }
    }
    public String enterSearchTerm() {
        System.out.print("Please enter the term you want to search for or !exit! to exit: ");
        return new Scanner(System.in).nextLine();
    }
    public void list(String list) {
        cleanScreen();
        System.out.println(list + "\n");
    }
}
