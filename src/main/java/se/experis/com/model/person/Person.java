package se.experis.com.model.person;

import com.github.javafaker.Faker;

import java.util.Date;

/**
 * Person class.
 * Contains information which is used to describe the person in a contact book.
 */
public class Person {
    private final String fullName;
    private final String address;
    private final String mobileNumber;
    private final String workNumber;
    private final Date birthday;

    public Person(String fullName, String address, String mobileNumber, String workNumber, Date birthday) {
        this.fullName = fullName;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.workNumber = workNumber;
        this.birthday = birthday;
    }

    /**
     * Constructor for when a person is created without information.
     * The information will be randomized using the Fake library.
     */
    public Person() {
        Faker fake = new Faker();
        this.birthday = fake.date().birthday(18, 98);
        this.fullName = fake.name().fullName();
        this.address = fake.address().fullAddress();
        this.mobileNumber = fake.phoneNumber().cellPhone();
        if((fake.number().numberBetween(0,100) < 10)) {
            this.workNumber = fake.phoneNumber().phoneNumber();
        } else {
            this.workNumber = "";
        }
    }

    /**
     * Searches all the persons variables for the searchterm and highlights it if its found.
     * @param searchTerm - The term which will be compared to each variable assigned to Person.
     * @return - Either a empty string if the search term wasn't found or a full string highlighting the searched term within the Persons variables.
     */
    public String containsInfo(String searchTerm) {
        String highlightedSearchTerm = "\u001b[31m" + searchTerm + "\u001b[0m";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--\n");
        stringBuilder.append(fullName.replaceAll(searchTerm, highlightedSearchTerm ));
        stringBuilder.append("\n\tAdress:\t");
        stringBuilder.append(address.replaceAll(searchTerm, highlightedSearchTerm ));
        stringBuilder.append("\n\tMobile number:\t");
        stringBuilder.append(mobileNumber.replaceAll(searchTerm, highlightedSearchTerm ));
        if(!workNumber.equals("")) {
            stringBuilder.append("\n\tWork number:\t");
            stringBuilder.append(workNumber.replaceAll(searchTerm, highlightedSearchTerm));
        }
        String birth = birthday.getDay() + "/" + birthday.getMonth() + "/" + birthday.getYear();
        stringBuilder.append("\n\tBirthday:\t" );
        stringBuilder.append(birth.replaceAll(searchTerm, highlightedSearchTerm ));
        stringBuilder.append("\n");
        if(fullName.contains(searchTerm) || address.contains(searchTerm) || mobileNumber.contains(searchTerm) || workNumber.contains(searchTerm) || birthday.toString().contains(searchTerm)) {
            return stringBuilder.toString();
        }
        return "";
    }
}
