package se.experis.com.model.exceptions;

public class BadContactsException extends Exception {
    public BadContactsException(String message) {
        super(message);
    }
}
