package se.experis.com.model.exceptions;

public class JsonFileFormatException extends Exception {
    public JsonFileFormatException(String message) {
        super(message);
    }
}
