package se.experis.com.model.contactbook;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import se.experis.com.model.exceptions.BadContactsException;
import se.experis.com.model.helper.ProgressRunner;
import se.experis.com.model.jsonhandler.JsonHandler;
import se.experis.com.model.person.Person;

import java.io.*;
import java.util.ArrayList;

public class ContactBook {
    private ArrayList<Person> contacts;
    private final String fileName = "Contacts.json";

    /**
     * Tries to load a ArrayList of type Person into the local variable contacts.
     * Uses the TypeToken to actually get the type of the ArrayList with the typing Person instead of just getting the class type of ArrayList.
     * @throws Exception - Any errors regarding the saving.
     */
    @SuppressWarnings({"unchecked"}) //We controll if the objects imported from json is of type Person.
    public void loadContactsFromJson() throws Exception {
        try {
            ProgressRunner.setProgress(true); //To indicate that the loading bar shoudl start.
            contacts = (ArrayList<Person>) JsonHandler.arrayListFromJsonFile(fileName, new TypeToken<ArrayList<Person>>() {
            }.getType(), Person.class);
            ProgressRunner.setProgress(false); //inidicate that the loading bar shoudl stop.
        }
        catch(Exception e) {
            ProgressRunner.setProgress(false);//inidicate that the loading bar shoudl stop.
            throw e;
        }
    }

    /**
     * Populates the ArrayList contacts with a random number of Persons.
     * Will initiate the ProgressRunner which helps the ui to count created persons.
     */
    public void populateArrayList() {
        int contactToCreate = (int) (Math.random()*500)+10;
        ProgressRunner.initiate(contactToCreate);
        contacts = new ArrayList<>();
        for(int i = 0; i < contactToCreate; i++) {
            contacts.add(new Person());
            ProgressRunner.add();
        }
    }

    /**
     * Will try to save the ArrayList contacts to a JSON file using JsonHandler.
     * @throws BadContactsException - If the ArrayList contacts is null or empty.
     * @throws IOException - Any errors regarding the saving of the JsonFile.
     */
    public void saveContactToJson() throws BadContactsException, IOException {
        if(contacts == null || contacts.size() < 1 ) {
            throw new BadContactsException("The contacts arraylist were empty or not initalized.");
        }
        JsonHandler.arrayListToJsonFile(contacts, fileName);
    }

    /**
     * Using the Person class containsInfo to create a string containing all Persons which has the searchTerm within its variables.
     * @param searchTerm - The term which should be searched for within all entries in the ArrayList contacts.
     *                   -
     * @return - A string containing all entities found containing the searchedTerm.
     */
    public String findAllWithSearchTerm(String searchTerm) {
        StringBuilder stringBuilder = new StringBuilder();
        int count = 0;
        int total = 0;
        for(Person p : contacts) {
            String info = p.containsInfo(searchTerm);
            stringBuilder.append(info);
            total++;
            if(!info.equals("")) {
                count ++;
            }
        }
        stringBuilder.append("[" + count + "/"+total+"] has the searchterm (" + searchTerm + ") in some variable.");
        return stringBuilder.toString();
    }
}
