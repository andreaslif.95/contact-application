package se.experis.com.model.jsonhandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import se.experis.com.model.exceptions.JsonFileFormatException;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class JsonHandler {
    /**
     * A generic ArrayList function which tries to load objects of the class c into a arrayList of Type type.
     * The function checks if the files exists and also if the objects withtin the json array is of the same class as the given Class c.
     * @param fileName - Name and path of the json file.
     * @param type - The actual type of the arrayList.
     * @param c - Class of the object which should be inside the ArrayList.
     * @return - A arraylist containing all the elements found within the JSON.
     * @throws JsonFileFormatException - If the class of objects withtin the JSON file does not correspond to the class of c.
     * @throws FileNotFoundException - If the file isn't found.
     * @throws IOException - If any reading errors occurs.
     */
    public static ArrayList<?> arrayListFromJsonFile(String fileName, Type type, Class<?> c) throws Exception {
        File file = new File(fileName);
        if(!file.exists()) {
            throw new FileNotFoundException();
        }
        else {
            ArrayList<Object> arrayList = new Gson().fromJson(new FileReader(file), type);
            if(arrayList.size() > 0) {
                if(arrayList.get(0).getClass().equals(c))
                    return arrayList;
            }
            throw new JsonFileFormatException("The JSON file were empty or did not contain the excepted object types.");
        }
    }

    /**
     * A function which saves a ArrayList to a Json file.
     * @param arrayList - The arrayList contating all the elements.
     * @param fileName - name of the file or the path to the file + filename
     * @return - true if it was sucessfully saved.
     * @throws IOException - For any reading errors.
     */
    public static boolean arrayListToJsonFile(ArrayList<?> arrayList, String fileName) throws IOException {
        File file = new File(fileName);
        if(!file.exists()) {
            if(!file.createNewFile()) {
                throw new IOException("The file could not be created..");
            }
        }
        //Creats a new GsonBuilder and sets it so that the file formating is nice looking.
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //Buffered writer to write the data to the json file.
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        //Actual json String.
        String json = gson.toJson(arrayList);
        //Writes data to file.
        bufferedWriter.write(json);
        //closes the file.
        bufferedWriter.close();
        return true;
    }
}
