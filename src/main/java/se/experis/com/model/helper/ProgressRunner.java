package se.experis.com.model.helper;

/**
 * Class used to help progressing the loading bars.
 */
public class ProgressRunner {
    private static int counter = -1;
    private static int destination = 0;
    private static boolean runProgress = false;
    public static void initiate(int dest) {
        destination = dest;
        counter = 0;
    }
    public static int[] getCount() {
        return new int[]{counter, destination}.clone();
    }
    public static void add() {
        counter++;
    }
    public static boolean runProgress() {
        return runProgress;
    }
    public static void setProgress(boolean state) {
        runProgress = state;
    }
}
