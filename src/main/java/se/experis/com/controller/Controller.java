package se.experis.com.controller;

import se.experis.com.model.contactbook.ContactBook;
import se.experis.com.model.exceptions.BadContactsException;
import se.experis.com.model.exceptions.JsonFileFormatException;
import se.experis.com.view.View;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Used to controll the flow of the program.
 */
public class Controller {
    ContactBook contactBook;
    View view;
    public Controller() {
        this.view = new View();
        this.contactBook = new ContactBook();
    }

    /**
     * Will try to load the file using contactBook.
     * If the file exists it will let the user search for words.
     * If the file does not exists it will create one and populate it.
     * Then let the user search for a search term.
     */
    public void start() {
        view.attemptToLoad();
        boolean createFile = true;
        //Tries to load the file, if any errors it will create a file and populate it.
        try {
            view.loadProgress();
            contactBook.loadContactsFromJson();
            createFile = false;
        } catch (JsonFileFormatException e) {
            if (!view.badJsonObjects()) {
                System.exit(1);
            }
        } catch (FileNotFoundException e) {
            view.fileNotFound();
        } catch (Exception e) {
            view.somethingWentWrong();
        }
        if(createFile) {
            view.creatingContactBookEntries();
            view.creationProgress();
            contactBook.populateArrayList();
            view.attemptingToCreateJson();
            try {
                contactBook.saveContactToJson();
            } catch (IOException | BadContactsException e) {
                view.fatalErrorExiting();
                System.exit(0);
            }
        }
        view.successfullyLoaded();
        //Looping and letting the user search for words until the user types '!exit!'.
        while(true) {
            String searchTerm = view.enterSearchTerm();
            if(searchTerm.contains("!exit!")) {
                break;
            }
            view.list(contactBook.findAllWithSearchTerm(searchTerm));
        }
    }
}
